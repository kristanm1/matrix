#ifndef _PERMUTATION_H_
#define _PERMUTATION_H_

class Permutation;

class Permutation {
    public:
        unsigned int *p, n;
        Permutation(unsigned int);
        ~Permutation();
        void display();
        unsigned int memAllocated();
        void permute(unsigned int, unsigned int);
        int parity();
        Permutation* inverse();

        static unsigned int numPermutation;
};

#endif