#include "../hdr/Permutation.h"
#include "../hdr/Matrix.h"
#include <stdio.h>

// CLASS MATRIX_LU
unsigned int MatrixLU::numMatrixLU = 0;

MatrixLU::MatrixLU(unsigned int h, unsigned int w): Matrix(h, w) {
    this->rank = 0;
    this->p = new Permutation(h);
    numMatrixLU++;
}

MatrixLU::MatrixLU(unsigned int h, unsigned int w, double *a): Matrix(h, w, a) {
    this->rank = 0;
    this->p = new Permutation(h);
    numMatrixLU++;
}

MatrixLU::~MatrixLU() {
    delete this->p;
    numMatrixLU--;
}

unsigned int MatrixLU::memAllocated() {
    return (unsigned int) (3*sizeof(unsigned int) + sizeof(double*) + (this->w*this->h)*sizeof(double) + this->p->memAllocated());
}

Matrix* MatrixLU::getL() {
    Matrix *L = new Matrix(this->h, (this->h < this->w) ? this->h : this->w);
    int i;
    for(i = 0; i < L->h; i++) {
        int j;
        for(j = 0; j < i; j++) {
            L->a[L->w*i + j] = this->a[this->w*this->p->p[i] + j];
        }
        L->a[L->w*i + i] = 1.0;
    }
    return L;
}

Matrix* MatrixLU::getU() {
    Matrix *U = new Matrix((this->h < this->w) ? this->h : this->w, this->w);
    int i;
    for(i = 0; i < this->h; i++) {
        int j;
        for(j = i; j < this->w; j++) {
            U->a[U->w*i + j] = this->a[this->w*this->p->p[i] + j];
        }
    }
    return U;
}

Matrix* MatrixLU::solve(Matrix *B) {
    if(this->h == this->w && B->h == this->h) {
        if(this->rank == this->h) {
            Matrix *X = B->permuteRows(this->p);
            int i;
            for(i = 0; i < this->h; i++) {
                int j;
                for(j = i-1; j >= 0; j--) {
                    int k;
                    for(k = 0; k < X->w; k++) {
                        X->a[X->w*i + k] -= X->a[X->w*j + k]*this->a[this->w*this->p->p[i] + j];
                    }
                }
            }
            for(i = this->h-1; i >= 0; i--) {
                int j;
                for(j = i+1; j < this->w; j++) {
                    int k;
                    for(k = 0; k < X->w; k++) {
                        X->a[X->w*i + k] -= X->a[X->w*j + k]*this->a[this->w*this->p->p[i] + j];
                    }
                }
                for(j = 0; j < X->w; j++) {
                    X->a[X->w*i + j] /= this->a[this->w*this->p->p[i] + i];
                }
            }
            return X;
        } else {
            printf("error -> MatrixLU::solve(Matrix*): singular matrix\n");
            return 0;
        }
    }
    printf("error -> MatrixLU::solve(Matrix*)\n");
    return 0;
}