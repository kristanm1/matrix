#include "../hdr/Permutation.h"
#include <stdio.h>

// CLASS PERMUTATION
unsigned int Permutation::numPermutation = 0;

Permutation::Permutation(unsigned int n) {
    this->p = new unsigned int[n];
    this->n = n;
    int i;
    for(i = 0; i < n; i++) {
        this->p[i] = i;
    }
    numPermutation++;
}

Permutation::~Permutation() {
    delete this->p;
    numPermutation--;
}

void Permutation::display() {
    printf("n=%u [", this->n);
    int i;
    for(i = 0; i < this->n; i++) {
        if(i == this->n-1) {
            printf("%u]\n", this->p[i]);
        } else {
            printf("%u, ", this->p[i]);
        }
    }
}

unsigned int Permutation::memAllocated() {
    return (unsigned int) ((1 + this->n)*sizeof(unsigned int) + sizeof(unsigned int*));
}

void Permutation::permute(unsigned int i, unsigned int j) {
    if(i < this->n && j < this->n) {
        unsigned int temp = this->p[i];
        this->p[i] = this->p[j];
        this->p[j] = temp;
    } else {
        printf("error -> Permutation::permute(unsigned int, unsigned int)\n");
    }
}

// return inverse permutation as new Permutation*
Permutation* Permutation::inverse() {
    Permutation *pi = new Permutation(this->n);
    int i;
    for(i = 0; i < this->n; i++) {
        pi->p[this->p[i]] = i;
    }
    return pi;
}

// return parity: 1(even) or -1(odd)
int Permutation::parity() {
    unsigned int i = p[0], start = 0, temp, parity = 1;
    p[0] += this->n;
    while(true) {
        while(i != start) {
            parity *= -1;
            temp = p[i];
            p[i] += this->n;
            i = temp;
        }
        for(i = 0; i < this->n && p[i] >= this->n; i++) {}
        if(i == this->n) {
            break;
        }
        start = i;
        i = p[i];
        p[start] += this->n;
    }
    for(i = 0; i < this->n; i++) {
        this->p[i] -= this->n;
    }
    return parity;
}