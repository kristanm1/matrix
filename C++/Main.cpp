#include "./hdr/MemInfo.h"
#include "./hdr/Matrix.h"
#include "./hdr/Permutation.h"
#include <stdio.h>

void test() {

    Matrix *A, *B, *X;

    double a1[] = {
        1,  1, 2,  7,
        12, 3, 6, 88,
        2,  3, 6, 18,
        1,  1, 3, 11
    };

    double b1[] = {
        41, 1,
        12, 2,
        -3, 4,
        40, 91
    };

    A = new Matrix(4, 4, a1);
    B = new Matrix(4, 2, b1);
    X = A->solve(B);

    delete X;
    delete A;
    delete B;

    
    double a2[] = {
        1, 1, 0, 1,
        0, 1, 0, 2,
        2, 1, 0, 1,
        0, 1, 1, 2,
        1, 2, 0, 3
    };

    double b2[] = {
        1,
        2,
        3,
        4,
        5
    };

    A = new Matrix(5, 4, a2);
    B = new Matrix(5, 1, b2);
    X = A->solve(B);

    delete X;
    delete A;
    delete B;


    double a3[] = {
        1, 1, 0, 1,
        1, 2, 0, 2,
        2, 1, 4, 1
    };

    double b3[] = {
        1, 4,
        2, 5,
        3, 6
    };

    A = new Matrix(3, 4, a3);
    B = new Matrix(3, 2, b3);
    X = A->solve(B);

    delete X;
    delete A;
    delete B;

    

    displayMemUsed();

}

int main() {

    test();
    return 0;

}