#include "../hdr/MemInfo.h"
#include "../hdr/Permutation.h"
#include "../hdr/Matrix.h"
#include <stdio.h>

void displayMemUsed() {
    printf("----------------------\n");
    printf("numPermutation: %4d |\n", Permutation::numPermutation);
    printf("numMatrix:      %4d |\n", Matrix::numMatrix);
    printf("numMatrixLU:    %4d |\n", MatrixLU::numMatrixLU);
    printf("numMatrixQR:    %4d |\n", MatrixQR::numMatrixQR);
    printf("----------------------\n");
}

