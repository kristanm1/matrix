#include "../hdr/Permutation.h"
#include "../hdr/Matrix.h"
#include <stdio.h>

// CLASS MATRIX_QR
unsigned int MatrixQR::numMatrixQR = 0;

MatrixQR::MatrixQR(unsigned int h, unsigned int w, double *a): Matrix(h, w, a) {
    this->rank = 0;
    this->r = new double[((h*(h+1))-((h-w)*(h-w+1)))/2];
    this->p = new Permutation(w);
    numMatrixQR++;
}

MatrixQR::~MatrixQR() {
    delete this->r;
    delete this->p;
    numMatrixQR--;
}

void MatrixQR::display() {
    int i;
    printf("R:\n");
    printf("size: %u x %u\n", this->h, this->w);
    for(i = 0; i < this->h; i++) {
        int j;
        for(j = 0; j < this->w; j++) {
            printf("%5.2f ", this->a[this->w*i + this->p->p[j]]);
        }
        printf("\n");
    }
    this->p->display();
    printf("householder rotations:\n");
    int rStart = 0;
    for(i = 0; i < this->w; i++) {
        int j;
        for(j = 0; j < this->h-i; j++) {
            printf("%6.3f ", this->r[rStart+j]);
        }
        printf("\n");
        rStart += (this->h - i);
    }
}

unsigned int MatrixQR::memAllocated() {
    unsigned int rSize = ((this->h*(this->h+1))-((this->h-this->w)*(this->h-this->w+1)))/2;
    return (unsigned int) (3*sizeof(unsigned int) + 2*sizeof(double*) + (this->w*this->h + rSize)*sizeof(double) + this->p->memAllocated());
}

Matrix* MatrixQR::mulQtranspose(Matrix *R) {
    if(R->h == this->h) {
        int i, rStart = 0;
        for(i = 0; i < this->w; i++) {
            double vNorm = 0.0;
            int j;
            for(j = i; j < this->h; j++) {
                vNorm += this->r[rStart+j-i]*this->r[rStart+j-i];
            }
            if(vNorm > TOL) {
                for(j = 0; j < R->w; j++) {
                    double dot = 0.0;
                    int k;
                    for(k = i; k < this->h; k++) {
                        dot += this->r[rStart+k-i]*R->a[R->w*k + j];
                    }
                    for(k = i; k < this->h; k++) {
                        R->a[R->w*k + j] -= 2*dot*this->r[rStart+k-i];
                    }
                }
            }
            rStart += (this->h - i);
        }
        return R;
    }
    printf("error -> MatrixQR::mulQtranspose(Matrix*)\n");
    return 0;
}

Matrix* MatrixQR::mulQ(Matrix *R) {
    if(R->h == this->h) {
        int i, rStart = ((this->h*(this->h+1)) - ((this->h-this->w)*(this->h-this->w+1)))/2;
        for(i = this->w-1; i >= 0; i--) {
            rStart -= (this->h - i);
            double vNorm = 0.0;
            int j;
            for(j = i; j < this->h; j++) {
                vNorm += this->r[rStart+j-i]*this->r[rStart+j-i];
            }
            if(vNorm > TOL) {
                for(j = 0; j < R->w; j++) {
                    double dot = 0.0;
                    int k;
                    for(k = i; k < this->h; k++) {
                        dot += this->r[rStart+k-i]*R->a[R->w*k + j];
                    }
                    for(k = i; k < this->h; k++) {
                        R->a[R->w*k + j] -= 2*dot*this->r[rStart+k-i];
                    }
                }
            }   
        }
        return R;
    }
    printf("error -> MatrixQR::mulQ(Matrix*)\n");
    return 0;
}

Matrix* MatrixQR::solveOverDetermined(Matrix *B) {
    if(this->h == B->h) {
        if(this->rank == this->w) {
            Matrix *Xfull = this->mulQtranspose(B->copy());
            Matrix *X = new Matrix(this->w, Xfull->w, Xfull->a);
            delete Xfull;
            int i;
            for(i = this->w-1; i >= 0; i--) {   
                int j;
                for(j = i+1; j < this->w; j++) {
                    int k;
                    for(k = 0; k < X->w; k++) {
                        X->a[X->w*i + k] -= X->a[X->w*j + k]*this->a[this->w*this->p->p[i] + j];
                    }
                }
                for(j = 0; j < X->w; j++) {
                    X->a[X->w*i + j] /= this->a[this->w*this->p->p[i] + i];
                }
            }
            return X;
        } else {
            printf("error -> MatrixQR::solveOverDetermined(Matrix*): singular matrix\n");
            return 0;
        }
    }
    printf("error -> MatrixQR::solveOverDetermined(Matrix*)\n");
    return 0;
}

Matrix* MatrixQR::solveUnderDetermined(Matrix *B) {
    if(this->w == B->h) {
        if(this->rank == this->w) {
            Matrix *X = new Matrix(this->h, B->w, B->a);
            Matrix *R = new Matrix(this->w, this->w, this->a);
            R->transposeI();
            int i;
            for(i = 0; i < R->w; i++) {   
                int j;
                for(j = 0; j < i; j++) {
                    int k;
                    for(k = 0; k < X->w; k++) {
                        X->a[X->w*i + k] -= X->a[X->w*j + k]*R->a[R->w*this->p->p[i] + j];
                    }
                }
                for(j = 0; j < X->w; j++) {
                    X->a[X->w*i + j] /= R->a[R->w*this->p->p[i] + i];
                }
            }
            delete R;
            return this->mulQ(X);
        } else {
            printf("error -> MatrixQR::solveUnderDetermined(Matrix*): singular matrix\n");
            return 0;
        }
    }
    printf("error -> MatrixQR::solveUnderDetermined(Matrix*)\n");
    return 0;
}
