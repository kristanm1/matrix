#include "../hdr/Permutation.h"
#include "../hdr/Matrix.h"
#include <stdio.h>
#include <cmath>

// CLASS MATRIX
unsigned int Matrix::numMatrix = 0;

// initialize new Matrix of size height x width
Matrix::Matrix(unsigned int h, unsigned int w) {
    this->h = h;
    this->w = w;
    this->a = new double[w*h];
    if(this->a != 0) {
        int i;
        for(i = 0; i < w*h; i++) {
            this->a[i] = 0.0;
        }
    } else {
        printf("error -> Matrix::Matrix(unsigned int, unsigned int)\n");
    }
    numMatrix++;
}

// initialize new Matrix of size height x width from array *a
Matrix::Matrix(unsigned int h, unsigned int w, double *a) {
    this->h = h;
    this->w = w;
    this->a = new double[w*h];
    if(this->a != 0) {
        int i;
        for(i = 0; i < w*h; i++) {
            this->a[i] = a[i];
        }
    } else {
        printf("error -> Matrix::Matrix(unsigned int, unsigned int, double*)\n");
    }
    numMatrix++;
}

// free memory
Matrix::~Matrix() {
    delete this->a;
    numMatrix--;
}

// display matrix to standart output
void Matrix::display() {
    printf("size: %u x %u\n", this->h, this->w);
    int i;
    for(i = 0; i < this->h; i++) {
        int j;
        for(j = 0; j < this->w; j++) {
            printf("%5.2f ", this->a[this->w*i + j]);
        }
        printf("\n");
    }
}

// display permuted rows of matrix to standart output
void Matrix::display(Permutation *p) {
    printf("size: %u x %u\n", this->h, this->w);
    int i;
    for(i = 0; i < this->h; i++) {
        int j;
        for(j = 0; j < this->w; j++) {
            printf("%5.2f ", this->a[this->w*p->p[i] + j]);
        }
        printf("\n");
    }
}

unsigned int Matrix::memAllocated() {
    return (unsigned int ) (2*sizeof(unsigned int) + sizeof(double*) + (this->w*this->h)*sizeof(double));
}

// get max value from matrix
double Matrix::max() {
    double max = this->a[0];
    int i;
    for(i = 1; i < this->h*this->w; i++) {
        if(max < this->a[i]) {
            max = this->a[i];
        }
    }
    return max;
}

// get abs. max value from matrix
double Matrix::absMax() {
    double max = (this->a[0] < 0) ? -this->a[0] : this->a[0], temp;
    int i;
    for(i = 1; i < this->h*this->w; i++) {
        temp = (this->a[i] < 0) ? -this->a[i] : this->a[i];
        if(max < temp) {
            max = temp;
        }
    }
    return max;
}

// get min value from matrix
double Matrix::min() {
    double min = this->a[0];
    int i;
    for(i = 1; i < this->h*this->w; i++) {
        if(this->a[i] < min) {
            min = this->a[i];
        }
    }
    return min;
}

// get abs. min value from matrix
double Matrix::absMin() {
    double min = (this->a[0] < 0) ? -this->a[0] : this->a[0], temp;
    int i;
    for(i = 1; i < this->h*this->w; i++) {
        temp = (this->a[i] < 0) ? -this->a[i] : this->a[i];
        if(temp < min) {
            min = temp;
        }
    }
    return min;
}

// create copy of matrix
Matrix* Matrix::copy() {
    return new Matrix(this->h, this->w, this->a);
}

// sum matrix this and A into matrix R
Matrix* Matrix::add(Matrix *A, Matrix *R) {
    if(A != 0 && R != 0) {
        if(A->h == R->h && A->w == R->w && A->h == this->h && A->w == this->w) {
            int i;
            for(i = 0; i < this->h*this->w; i++) {
                R->a[i] = this->a[i] + A->a[i];
            }
            return R;
        }
    }
    printf("error -> Matrix::add(Matrix*, Matrix*)\n");
    return 0;
}

// sum matrix this and A and return new matrix
Matrix* Matrix::add(Matrix *A) {
    if(A != 0 && this->h == A->h && this->w == A->w) {
        return this->add(A, new Matrix(this->h, this->w));
    } else {
        printf("error -> Matrix::add(Matrix*)\n");
        return 0;
    }
}

// from matrix this subtract matrix A and save result to matrix R
Matrix* Matrix::sub(Matrix *A, Matrix *R) {
    if(A != 0 && R != 0) {
        if(A->h == R->h && A->w == R->w && A->h == this->h && A->w == this->w) {
            int i;
            for(i = 0; i < this->h*this->w; i++) {
                R->a[i] = this->a[i] - A->a[i];
            }
            return R;
        }
    }
    printf("error -> Matrix::add(Matrix*, Matrix*)\n");
    return 0;
}

// from matrix this subtract matrix A and return new matrix
Matrix* Matrix::sub(Matrix *A) {
    if(A != 0 && this->h == A->h && this->w == A->w) {
        return this->sub(A, new Matrix(this->h, this->w));
    } else {
        printf("error -> Matrix::sub(Matrix*)\n");
        return 0;
    }
}

// multiply matrix this with scalar f and save result to matrix R
Matrix* Matrix::mul(double f, Matrix *R) {
    if(R != 0 && this->h == R->h && this->w == R->w) {
        int i;
        for(i = 0; i < this->h*this->w; i++) {
            R->a[i] = this->a[i] * f;
        }
        return R;
    }
    printf("error -> Matrix::mul(double, Matrix*)\n");
    return 0;
}

// multiply matrix this with scalar f and return new matrix
Matrix* Matrix::mul(double f) {
    return this->mul(f, new Matrix(this->h, this->w));
}

Matrix* Matrix::mul(Matrix *A, Matrix *R) {
    if(A != 0 && this->w == A->h && R != 0 && R->h == this->h && R->w == A->w) {
        int i;
        for(i = 0; i < R->h; i++) {
            int j;
            for(j = 0; j < R->w; j++) {
                double f = 0.0;
                int k;
                for(k = 0; k < this->w; k++) {
                    f += this->a[this->w*i + k] * A->a[A->w*k + j];
                }
                R->a[R->w*i + j] = f;
            }
        }
        return R;
    }
    printf("error -> Matrix::mul(Matrix*)\n");
    return 0;
}

// multiply matrix this with A and return new matrix
Matrix* Matrix::mul(Matrix *A) {
    if(A != 0 && this->w == A->h) {
        return this->mul(A, new Matrix(this->h, A->w));
    }
    printf("error -> Matrix::operator*(Matrix*)\n");
    return 0;
}

// transpose matrix this
Matrix* Matrix::transposeI() {
    int i;
    for(i = 0; i < this->h*this->w; i++) {
        int j = (i%this->h)*this->w+(i/this->h);
        while(j < i) {
            j = (j%this->h)*this->w+(j/this->h);
        }
        double temp = this->a[i];
        this->a[i] = this->a[j];
        this->a[j] = temp;
    }
    i = this->h;
    this->h = this->w;
    this->w = i;
    return this;
}

// transpose matrix and return new matrix
Matrix* Matrix::transpose() {
    Matrix *A = this->copy();
    return A->transpose();
}

Matrix* Matrix::permuteRowsI(Permutation *p) {
    if(this->h == p->n) {
        int i;
        for(i = 0; i < p->n; i++) {
            int pos = p->p[i];
            while(pos < i) {
                pos = p->p[pos];
            }
            if(pos != i) {
                int j;
                for(j = 0; j < this->w; j++) {
                    double temp = this->a[this->w*i + j];
                    this->a[this->w*i + j] = this->a[this->w*pos + j];
                    this->a[this->w*pos + j] = temp;
                }
            }
        }
        return this;
    }
    printf("error -> Matrix::permuteRowsI(Permutation)\n");
    return 0;
}

Matrix* Matrix::permuteRows(Permutation *p) {
    if(p != 0) {
        return this->copy()->permuteRowsI(p);
    }
    printf("error -> Matrix::permuteRows(Permutation)\n");
    return 0;
}

// return matrix rank as integer
unsigned int Matrix::rank() {
    Matrix A = Matrix(this->h, this->w, this->a);
    Permutation p = Permutation(this->h);
    int pr = 0, pc = 0, maxIdx;
    double max, temp;
    while(pr < A.h && pc < A.w) {
        maxIdx = pr;
        max = (A.a[A.w*p.p[pr] + pc] < 0) ? -A.a[A.w*p.p[pr] + pc] : A.a[A.w*p.p[pr] + pc];
        int i;
        for(i = pr+1; i < A.h; i++) {
            temp = (A.a[A.w*p.p[i] + pc] < 0) ? -A.a[A.w*p.p[i] + pc] : A.a[A.w*p.p[i] + pc];
            if(max < temp) {
                max = temp;
                maxIdx = i;
            }
        }
        if(max > TOL) {
            if(pr != maxIdx) {
                p.permute(pr, maxIdx);
            }
            for(i = pr + 1; i < A.h; i++) {
                temp = A.a[A.w*p.p[i] + pc]/A.a[A.w*p.p[pr] + pc];
                int j;
                for(j = pc; j < A.w; j++) {
                    A.a[A.w*p.p[i] + j] -= temp*A.a[A.w*p.p[pr] + j];
                }   
            }
            pr++;
        }
        pc++;
    }
    return pr;
}

Matrix* Matrix::solve(Matrix *B) {
    Matrix *X = 0;
    if(this->h == this->w) {
        MatrixLU *lu = this->lu();
        X = lu->solve(B);
        delete lu;
    } else {
        MatrixQR *qr;
        if(this->h > this->w) {
            qr = this->qr();
            X = qr->solveOverDetermined(B);
        } else {
            this->transposeI();
            qr = this->qr();
            X = qr->solveUnderDetermined(B);
            this->transposeI();
        }
        delete qr;
    }
    return X;
}

// return LU decomposition of matrix via doolittle algorithm: PA = LU
MatrixLU* Matrix::lu() {
    MatrixLU *A = new MatrixLU(this->h, this->w, this->a);
    int pr = 0, pc = 0;
    while(pr < this->h && pc < this->w) {
        int j, maxIdx = pr;
        double max = (A->a[A->w*A->p->p[pr] + pc] < 0) ? -A->a[A->w*A->p->p[pr] + pc] : A->a[A->w*A->p->p[pr] + pc], temp;
        for(j = pr+1; j < A->h; j++) {
            temp = (A->a[A->w*A->p->p[j] + pc] < 0) ? -A->a[A->w*A->p->p[j] + pc] : A->a[A->w*A->p->p[j] + pc];
            if(max < temp) {
                max = temp;
                maxIdx = j;
            }
        }
        if(max >= TOL) {
            if(maxIdx != pr) {
                A->p->permute(maxIdx, pr);
            }
            for(j = pr+1; j < A->h; j++) {
                temp = A->a[A->w*A->p->p[j] + pc]/A->a[A->w*A->p->p[pr] + pc];
                int k;
                for(k = pc; k < A->w; k++) {
                    A->a[A->w*A->p->p[j] + k] -= temp*A->a[A->w*A->p->p[pr] + k];
                }
                A->a[A->w*A->p->p[j] + pc] = temp;
            }
            pr++;
            pc++;
        } else {
            pc++;
        }
    }
    A->rank = pr;
    return A;
}

// return determinant of matrix if height == width
double Matrix::determinant() {
    if(this->h == this->w) {
        MatrixLU *lu = this->lu();
        if(lu != 0) {
            int i;
            double determinant = lu->p->parity();
            for(i = 0; i < this->h; i++) {
                determinant *= lu->a[lu->w*lu->p->p[i] + i];
            }
            delete lu;
            return determinant;
        }
        return 0.0;
    } else {
        printf("error -> Matrix::determinant()\n");
        return 0.0;
    }
}

MatrixQR* Matrix::qr() {
    if(this->h >= this->w) {
        MatrixQR *A = new MatrixQR(this->h, this->w, a);
        int i, rStart = 0;
        for(i = 0; i < A->w; i++) {
            int j;
            double vNorm = 0.0;
            for(j = i; j < A->h; j++) {
                A->r[rStart+j-i] = A->a[A->w*j + A->p->p[i]];
                vNorm += A->a[A->w*j + A->p->p[i]]*A->a[A->w*j + A->p->p[i]];
            }
            if(A->a[A->w*i + A->p->p[i]] >= 0) {
                A->r[rStart] += sqrt(vNorm);
            } else {
                A->r[rStart] -= sqrt(vNorm);
            }
            vNorm = 0.0;
            for(j = i; j < A->h; j++) {
                vNorm += A->r[rStart+j-i]*A->r[rStart+j-i];
            }

            // TO-DO: pivotiranje vrstic: AP = QR -> bolj numericnm stabilno

            vNorm = sqrt(vNorm);
            if(vNorm > TOL) {
                A->rank++;
                for(j = i; j < A->h; j++) {
                    A->r[rStart+j-i] /= vNorm;
                }
                for(j = i; j < A->w; j++) {
                    double dot = 0.0;
                    int k;
                    for(k = i; k < A->h; k++) {
                        dot += A->r[rStart+k-i]*A->a[A->w*k + A->p->p[j]];
                    }
                    for(k = i; k < A->h; k++) {
                        A->a[A->w*k + A->p->p[j]] -= 2*dot*A->r[rStart+k-i];
                    }
                }
            }
            rStart += (this->h - i);
        }
        return A;
    }
    printf("error -> Matrix::qr()\n");
    return 0;
}

Matrix* Matrix::identity(unsigned int h, unsigned int w) {
    Matrix *I = new Matrix(h, w);
    int i, iMax = (h < w) ? h : w;
    for(i = 0; i < iMax; i++) {
        I->a[I->w*i + i] = 1;
    }
    return I;
}