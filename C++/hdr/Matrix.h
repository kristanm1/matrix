#ifndef _MATRIX_H_
#define _MATRIX_H_

#define TOL 1e-15

class Permutation;
class MatrixLU;
class MatrixQR;

class Matrix {
    public:
        unsigned int w, h;
        double *a;
        Matrix(unsigned int, unsigned int);
        Matrix(unsigned int, unsigned int, double*);
        virtual ~Matrix();
        virtual void display();
        virtual void display(Permutation*);
        virtual unsigned int memAllocated();
        double max();
        double absMax();
        double min();
        double absMin();
        virtual Matrix* copy();
        Matrix* add(Matrix*, Matrix*);
        Matrix* add(Matrix*);
        Matrix* sub(Matrix*, Matrix*);
        Matrix* sub(Matrix*);
        Matrix* mul(double, Matrix*);
        Matrix* mul(double);
        Matrix* mul(Matrix*, Matrix*);
        Matrix* mul(Matrix*);
        Matrix* transposeI();
        Matrix* transpose();
        Matrix* permuteRowsI(Permutation*);
        Matrix* permuteRows(Permutation*);
        unsigned int rank();
        Matrix* solve(Matrix*);
        MatrixLU* lu();
        double determinant();
        MatrixQR* qr(); // TO-DO: add column pivoring for better numerical stability

        static unsigned int numMatrix;
        static Matrix* identity(unsigned int, unsigned int);
};

class MatrixLU: public Matrix {
    public:
        unsigned int rank;
        Permutation *p;
        MatrixLU(unsigned int, unsigned int);
        MatrixLU(unsigned int, unsigned int, double*);
        ~MatrixLU();
        unsigned int memAllocated();
        Matrix* getL();
        Matrix* getU();
        Matrix* solve(Matrix*);

        static unsigned int numMatrixLU;
};

class MatrixQR: public Matrix {
    public:
        unsigned int rank;
        double *r;
        Permutation *p;
        MatrixQR(unsigned int, unsigned int, double*);
        ~MatrixQR();
        void display();
        unsigned int memAllocated();
        Matrix* mulQtranspose(Matrix*);
        Matrix* mulQ(Matrix*);
        Matrix* solveOverDetermined(Matrix*);
        Matrix* solveUnderDetermined(Matrix*);

        static unsigned int numMatrixQR;
};

#endif