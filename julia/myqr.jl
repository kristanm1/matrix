using LinearAlgebra

A = [
    12 -51   4;
     6 167 -68;
    -4  24 -41.0
]
bA = [
    1;
    2;
    3.0
]

B = [
    1 1 0 1;
    0 1 0 2;
    2 1 0 1;
    0 1 3 2;
    1 2 0 3.0;
]
bB = [
    1;
    2;
    3;
    4;
    5.0
]
bB = reshape(bB, length(bB), 1)

C = [
    1 1 0 1;
    0 1 0 2;
    2 1 0 1.0
]
bC = [
    1 4;
    2 5;
    3 6.0
]
bC = reshape(bC, length(bC[:, 1]), length(bC[1, :]))


function mySign(x)
    if x >= 0
        return 1.0;
    else
        return -1.0;
    end
end

function householderQR1(A; TOL=1e-12)
    m, n = size(A)
    if m >= n
        R = copy(A)
        Q = Matrix{Float64}(I, m, m)
        for i = 1:n
            v = R[i:end, i]
            v = reshape(v, m-i+1, 1)
            v[1] = v[1] + mySign(R[i, i])*norm(v, 2);
            if dot(v, v) > TOL
                P = Matrix{Float64}(I, m-i+1, m-i+1) - 2*(v*v')./dot(v, v)
                R[i:end, i:end] = P*R[i:end, i:end]
                R[i+1:end, i] .= 0

                Qp = Matrix{Float64}(I, m, m)
                Qp[i:end, i:end] = P'
                Q = Q*Qp
            end
        end
        return R, Q
    else
        return [], []
    end
end


function householderQR2(A; TOL=1e-12)
    m, n = size(A)
    if m >= n
        R = copy(A)
        U = zeros(m, n)
        for i = 1:n
            v = R[i:end, i]
            v = reshape(v, m-i+1, 1)
            v[1] = v[1] + mySign(R[i, i])*norm(v, 2);
            if norm(v, 2) > TOL
                v = v/norm(v, 2)
                U[i:end, i] = v
                for j = i:n
                    R[i:end, j] = R[i:end, j] - 2*v*dot(v, R[i:end, j])
                end
                if i+1 <= m
                    R[i+1:end, i] .= 0
                end
            end
        end
        return R, U
    else
        return [], []
    end
end

function getQ(U; TOL=1e-12)
    m, n = size(U)
    Q = Matrix{Float64}(I, m, m)
    for i = n:-1:1
        v = reshape(U[i:end, i], m-i+1, 1)
        if dot(v, v) > TOL
            for j = i:m
                Q[i:end, j] = Q[i:end, j] - 2*(v*dot(v, Q[i:end, j]))
            end
        end
    end
    return Q
end

function getQthin(U; TOL=1e-12)
    m, n = size(U)
    Q = Matrix{Float64}(I, m, n)
    for i = n:-1:1
        v = reshape(U[i:end, i], m-i+1, 1)
        if dot(v, v) > TOL
            for j = i:n
                Q[i:end, j] = Q[i:end, j] - 2*(v*dot(v, Q[i:end, j]))
            end
        end
    end
    return Q
end

function mulQ(U, A; TOL=1e-12)
    m, n = size(A)
    mu, nu = size(U)
    B = copy(A)
    for i = 1:nu
        v = reshape(U[i:end, i], m-i+1, 1)
        if dot(v, v) > TOL
            for j = 1:n
                B[i:end, j] = B[i:end, j] - 2*(v*dot(v, B[i:end, j]))
            end
        end
    end
    return B
end

function mulQthin(U, A; TOL=1e-12) #TODO...
    m, n = size(A)
    mu, nu = size(U)
    B = copy(A)
    for i = 1:nu
        v = reshape(U[i:end, i], m-i+1, 1)
        if dot(v, v) > TOL
            for j = 1:n
                B[i:end, j] = B[i:end, j] - 2*(v*dot(v, B[i:end, j]))
            end
        end
    end
    return B
end


M = B
m, n = size(M)

Q, R = qr(M)
R1, Q1 = householderQR1(M)
R2, U = householderQR2(M)
Q2 = getQ(U)

#display(R1)
#display(R2)

#display(Q1)
#display(Q2)

Q2thin = getQthin(U)
#display(Q2thin)

#display(Q2'*bB)
#display(mulQ(U, bB))

#display(Q2thin'*bB)
#display(mulQthin(U, bB))


R3, U3 = householderQR2(C')
Q3 = getQ(U3)

b = zeros(length(R3[:, 1]), length(bC[1, :]))
b[1:length(R3[1, :]), :] = (R3[1:3, :]'\bC)
display(Q3*b)

display(C\bC)

display("END");
